USE foodieDevDB;

CREATE TABLE profiles
(
  user_id         INT unsigned NOT NULL AUTO_INCREMENT,
  first_name      VARCHAR(150) NOT NULL,
  last_name       VARCHAR(150) NOT NULL,
  email           VARCHAR(150) NOT NULL,
  password        VARCHAR(150) NOT NULL,
  PRIMARY KEY     (user_id)
),
CREATE TABLE recipes
(
  entry_num       INT unsigned NOT NULL,
  name            VARCHAR(50) NOT NULL,
  time            TIME NOT NULL,
  recipe_link     VARCHAR(150) NOT NULL,
  user_id         INT unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY     (entry_num),
  FOREIGN KEY     (user_id) REFERENCES profiles(user_id)
);