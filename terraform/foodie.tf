terraform {
  backend "s3" {
    bucket               = "tf-state-bucket-1"
    key                  = "tf-state/terraform.tfstate"
    region               = "us-east-1"
    dynamodb_table       = ""
    workspace_key_prefix = "env"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

locals {
  dev-vpc = aws_vpc.foodie-dev-vpc.id
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_vpc" "foodie-dev-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    "Name" : "foodie-dev-vpc"
  }
}

resource "aws_security_group" "foodie-dev-sg" {
  name   = "foodie-dev-sg"
  vpc_id = local.dev-vpc
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [aws_vpc.foodie-dev-vpc.cidr_block, "0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "Name" : "food-dev-sg"
  }
}

resource "aws_internet_gateway" "foodie-dev-ig" {
  vpc_id = local.dev-vpc
  tags = {
    "Name" : "food-dev-ig"
  }
}

resource "aws_subnet" "dev-private-1" {
  cidr_block        = "10.0.1.0/24"
  vpc_id            = local.dev-vpc
  availability_zone = "us-east-1a"
  tags = {
    "Name" : "dev-private-1"
  }
}

resource "aws_subnet" "dev-private-2" {
  cidr_block        = "10.0.2.0/24"
  vpc_id            = local.dev-vpc
  availability_zone = "us-east-1b"
  tags = {
    "Name" : "dev-private-2"
  }
}

resource "aws_subnet" "dev-public" {
  cidr_block              = "10.0.0.0/24"
  vpc_id                  = local.dev-vpc
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true
  tags = {
    "Name" : "dev-public"
  }
}

resource "aws_route_table" "foodie-dev-rt" {
  vpc_id = local.dev-vpc
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.foodie-dev-ig.id
  }
  tags = {
    "Name" : "foodie-dev-rt"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.dev-public.id
  route_table_id = aws_route_table.foodie-dev-rt.id
}

resource "aws_key_pair" "foodie-dev-key" {
  key_name   = "foodie-dev-key"
  public_key = var.dev_pub_ec2_ssh
}

resource "aws_instance" "dev-frontend-instance" {
  ami             = var.ami_id
  instance_type   = "t2.micro"
  subnet_id       = aws_subnet.dev-public.id
  key_name        = "foodie-dev-key"
  security_groups = [aws_security_group.foodie-dev-sg.id]
  tags = {
    "Name" : "dev-frontend-instance"
  }
}

resource "aws_db_subnet_group" "foodie-dev-db-subnet-group" {
  name        = "foodie-dev-db-subnet-group"
  description = "DB subnet group for the foodie dev DB"
  subnet_ids  = [aws_subnet.dev-private-1.id, aws_subnet.dev-private-2.id]

  tags = {
    "Name" : "foodie-dev-db-subnet-group"
  }
}

resource "aws_db_instance" "foodie-dev-db" {
  identifier            = "foodie-dev-db"
  allocated_storage     = 20
  max_allocated_storage = 1000
  storage_type          = "gp2"
  engine                = "mysql"
  engine_version        = "8.0.20"
  instance_class        = "db.t2.micro"

  name                 = "foodieDevDB"
  username             = "admin"
  password             = var.dev_db_pass
  parameter_group_name = "default.mysql8.0"
  availability_zone    = "us-east-1b"

  vpc_security_group_ids = [aws_security_group.foodie-dev-sg.id]
  db_subnet_group_name   = aws_db_subnet_group.foodie-dev-db-subnet-group.name

  skip_final_snapshot = true
}