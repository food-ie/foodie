#!/usr/bin/env bash

# install net tools
yum install nettools

# setup java 15 on host
wget https://jdk.java.net/15/openjdk-15.0.2_linux-x64_bin.tar.gz
tar zxvf openjdk-15.0.2_linux-x64_bin.tar.gz
mv jdk-15.0.2 /usr/local/
cat << EOT >> ~/.bashrc
PATH=/usr/local/jdk-15.0.2/bin:$PATH
export PATH
EOT
source .bash_profile

# forward incoming port 80 to server port 8080 on the test env
iptables -A INPUT -i eth0 -p tcp --dport 80 -j ACCEPT
iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080


# config the file directory for the first time
if [ ! -d "/opt/foodie" ]; then
  mkdir /opt/foodie && mkdir /opt/foodie/var
else
  echo "foodie directory already exists"
fi

if [ -f "/home/ec2-user/foodie-0.0.1-SNAPSHOT.jar" ]; then
  mv /home/ec2-user/foodie-0.0.1-SNAPSHOT.jar /opt/foodie-0.0.1-SNAPSHOT.jar
else
  echo "jar ready to execute"
fi

# run the jar
java -jar /opt/foodie-0.0.1-SNAPSHOT.jar