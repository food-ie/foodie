#!/bin/env bash

# remove the current snapshot
echo "cleaning up old jar and output"
rm -f /opt/user/foodie-0.0.1-SNAPSHOT.jar
rm -f /home/ec2-user/nohup.out

echo "cleaning up old process"
# kill process if its currently running
fuser -k 8080/tcp

echo "copying new jar to app folder"
# copy the new snapshot and start the spring server
sudo mv /home/ec2-user/foodie-0.0.1-SNAPSHOT.jar /opt/foodie/foodie-0.0.1-SNAPSHOT.jar

# restart the server
echo "starting web server....."
nohup java -jar /opt/foodie/foodie-0.0.1-SNAPSHOT.jar &

echo "server started...."